from flask import Flask, render_template, jsonify
import requests
import meteomatics.api as api
import logging
from meteomatics.logger import create_log_handler
from meteomatics._constants_ import LOGGERNAME
import datetime as dt
import matplotlib.pyplot as plt
import json

from openfoodfacts import openfoodfacts

app = Flask(__name__)

## CREDENTIALS

username = "ulco_zneep"
password = "F9V6c8Lri0"
###Input timeseries:
now = dt.datetime.utcnow().replace(hour=0, minute=0, second=0, microsecond=0)
startdate_ts = now
enddate_ts = startdate_ts + dt.timedelta(days=1)
interval_ts = dt.timedelta(hours=1)
coordinates_ts = [(50.948056, 1.856389)]
parameters_ts = ['t_2m:C', 'rr_1h:mm']
model = 'mix'
ens_select = None  # e.g. 'median'
cluster_select = None  # e.g. "cluster:1", see http://api.meteomatics.com/API-Request.html#cluster-selection
interp_select = 'gradient_interpolation'
LAT = '50.948056'
LON = '1.856389'

API_METEO = {
    'username': "ulco_zneep",
    'password': "F9V6c8Lri0",
    'API_URL': "api.meteomatics.com/",
    'API_KEY': "825614c2329f15283bba8894d20c2133",
    'LAT': '50.948056',
    'LON': '1.856389',
}

@app.route("/")
def home():
    return "API prévisions"


@app.route("/off")
def off():
    return jsonify(getProductFromEAN())


def getProductFromEAN(EAN):
    # OPEN FOOD FACT
    product = openfoodfacts.products.get_product(EAN)
    # print(product['product']['categories'])

    # categories = request(OPENFOODFACT, ean)
    # ://github.com/openfoodfacts/openfoodfacts-python
    # notre model predit
    # model.predict(data, categories).
    return product


@app.route('/meteo/')
def no_meteo():
    return "Error : Incorrect use of route meteo , try meteo/:EAN"


# Try with Cristaline ean : 3274080005003
@app.route('/meteo/<ean>/')
def meteo(ean="0"):
    print("ean : " + str(ean))
    # METEO PREDICTIONS
    data = getPredictionsFor10Days()
    #savePlotFromJson(data)
    product = getProductFromEAN(ean)
    result = {
        "ean": ean,
        "product_name": product["product"]["generic_name"],
        "categories": product['product']['categories'],
        "to_command": "2",
    }


    return jsonify(result)


def getPredictionsFor10Days():
    _logger = logging.getLogger(LOGGERNAME)
    # Date format YYYY-MM-DD
    URL = "https://api.meteomatics.com/{beginDate}T06:45:00.000+01:00--{endDate}T21:45:00.000+01:00:PT6H/t_min_2m_24h:C/51.5073219,-0.1276474/json?model=mix".format(
        beginDate=dt.datetime.utcnow().strftime("%Y-%m-%d"),
        endDate=(dt.datetime.utcnow() + dt.timedelta(days=9)).strftime("%Y-%m-%d"),
        lat=""
    )

    _logger.info("\ntime series:")
    df_ts = api.query_api(
        URL,
        username, password)
    return df_ts


def savePlotFromJson(data):
    data = data.json()
    datas = data['data'][0]
    coordinates = datas['coordinates'][0]

    dates = coordinates['dates']
    x = []
    y = []
    for v in dates:
        print(v)
        x.append(v['date'])
        y.append(v['value'])
    plt.plot(x, y)
    plt.savefig("graph.png")

# https://api.meteomatics.com/2022-03-09T06:45:00.000+01:00--2022-03-18T21:45:00.000+01:00:PT6H/t_min_2m_24h:C/51.5073219,-0.1276474/html?model=mix
